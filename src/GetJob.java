
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class GetJob {

	public static final String KEY = "KEY";
	public static final String SECRET = "SECRET";

	public void CheckImage(String xpath, WebDriver driver) throws Exception {
		// Displaying the total number of images in the Webpage
		System.out.println("xpaths " + xpath);
		List<WebElement> images = driver.findElements(By.xpath(xpath));

		System.out.println("Total links are " + images.size());

		for (int i = 0; i <= images.size(); i++) {
			try {
				// Iterating through the array list and getting the URL's
				String ImageFile = images.get(i).getAttribute("src");

				// print iamge src
				System.out.println("image src " + ImageFile);
				
				//TODO:  If you want to check the file name, use string function to verify

				Boolean ImagePresent = (Boolean) ((JavascriptExecutor) driver).executeScript(
						"return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0",
						ImageFile);
				if (!ImagePresent) {
					System.out.println("Image not displayed.");
				} else {
					System.out.println("Image displayed.");
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}

//	public static void main(String[] args) throws Exception {
//		WebDriver driver = new ChromeDriver();
//		String appUrl = "https://www.techvalidate.com/customers";
//		driver.get(appUrl);
//		
//		
//		System.out.println("app url  " + appUrl);
//		String xpath = "//div[@class='js-device-carousel-large']/img";
//		System.out.println("xpaths " + xpath);
//		
//		CheckImage(xpath, driver);
//		
//		
//		
//		List<WebElement> images = driver.findElements(By.xpath(xpath));
//
//		// Displaying the total number of images in the Webpage
//		System.out.println("Total links are " + images.size());
//
//		for (int i = 0; i <= images.size(); i++) {
//			try {
//				// Iterating through the array list and getting the URL's
//				String nextHref = images.get(i).getAttribute("src");
//
//				System.out.println("image src " + nextHref);
//
//			} catch (Exception e) {
//				System.out.println(e.getMessage());
//			}
//		}
//
//	}

	 public static void main(String[] args) throws Exception {
	
	 // objects and variables instantiation
	 WebDriver driver = new ChromeDriver();
	
	 String appUrl = "https://rollsroyce.wd3.myworkdayjobs.com/professional";
	 // String xpath= "//*[@data-automation-id=\"compositeContainer\"]";
	 String xpath = "//*[@data-automation-id=\"compositeContainer\"]/div/div";
	 String search = "//*[@data-automation-id=\"textInputBox\"]";
	
	 // launch the firefox browser and open the application url
	 driver.get(appUrl);
	 Robot robot = new Robot();
	 List<WebElement> titles = driver.findElements(By.xpath(xpath));
	 Actions action = new Actions(driver);
	 String copyUrl = "//div[@data-automation-id=\"copyUrl\"]";
	 WebElement searchBox = driver.findElement(By.xpath(search));
	 TextTransfer textTransfer = new TextTransfer();
	 for (WebElement title : titles) {
	 System.out.println(title.getText());
	 action.moveToElement(title).contextClick().build().perform();
	 WebElement elementCopy = driver.findElement(By.xpath(copyUrl));
	 elementCopy.click();
	 action.moveToElement(searchBox).sendKeys(Keys.chord(Keys.CONTROL,
	 "v")).build().perform();
	 log("Clipboard contains:" + textTransfer.getClipboardContents());
	
	 }
	
	 driver.close();
	 System.out.println("Test script executed successfully.");
	
	 // terminate the program
	 System.exit(0);
	 }
	public String getClipboardContents() {
		String result = "";
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		// odd: the Object param of getContents is not currently used
		Transferable contents = clipboard.getContents(null);
		boolean hasTransferableText = (contents != null) && contents.isDataFlavorSupported(DataFlavor.stringFlavor);
		if (hasTransferableText) {
			try {
				result = (String) contents.getTransferData(DataFlavor.stringFlavor);
			} catch (UnsupportedFlavorException | IOException ex) {
				System.out.println(ex);
				ex.printStackTrace();
			}
		}
		return result;
	}

	private static void log(String msg) {
		System.out.println(msg);
	}
}